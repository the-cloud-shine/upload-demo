import React from 'react';
import axios, { put } from 'axios';
import './App.css';

import Amplify from 'aws-amplify';
import awsconfig from './aws-exports';

Amplify.configure(awsconfig);

var SIGNED_URL_ENDPOINT = process.env.SIGNED_URL_ENDPOINT;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      file: null
    }
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    this.fileUpload = this.fileUpload.bind(this);
  }
  onFormSubmit(e){
    console.log('Form Submitted');
    e.preventDefault();
    this.fileUpload(this.state.file);
  }
  onChange(e) {
    console.log('Change Detected')
    this.setState({file:e.target.files[0]})
  }
  fileUpload(file){
    console.log('Uploading File')
    axios.get(SIGNED_URL_ENDPOINT, {
      params: {
        filename: file.name,
        filetype: file.type
      }
    })
    .then(function (result) {
      console.log(result)
      var signedUrl = result.data.signed_url;
      console.log('Signed URL: ' + signedUrl);
      const formData = new FormData();
      formData.append('file',file)
      const config = {
        headers: {
            'Content-Type': file.type
        }
      }
      return put(signedUrl, file, config);
    })
    .then(function (result) {
      console.log(result);
    })
    .catch(function (err) {
      console.log(err);
    });
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <form onSubmit={this.onFormSubmit}>
            <input type="file" onChange={this.onChange} />
            <button type="submit">Upload</button>
          </form>
        </header>
      </div>
    );
  };
}

export default App;
