'use strict';
console.log('Loading generate presigned URL function');
var AWS = require('aws-sdk');
var s3 = new AWS.S3({
  signatureVersion: 'v4',
});

var BUCKET_NAME = process.env.BUCKET_NAME;

exports.handler = (event, context, callback) => {
    
    const url = s3.getSignedUrl('putObject', {
      Bucket: BUCKET_NAME,
      Key: event.queryStringParameters.filename,
      Expires: 1000, //expiry time in sec
      ContentType: event.queryStringParameters.filetype
    });
    var responseCode = 200;

    console.log("request: " + JSON.stringify(event));
 
    var responseBody = {
        signed_url: url,
    };
    var response = {
        statusCode: responseCode,
        headers: {
            "Access-Control-Allow-Origin" : "*"
        },
        body: JSON.stringify(responseBody)
    };
    console.log("response: " + JSON.stringify(response))
  
    callback(null, response);
  
};